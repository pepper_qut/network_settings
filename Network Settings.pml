<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Network Settings" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="styles" src="html/css/styles.css" />
        <File name="brightness" src="html/images/brightness.png" />
        <File name="speaker_icon" src="html/images/speaker_icon.svg" />
        <File name="toggle_left" src="html/images/toggle_left.png" />
        <File name="toggle_off" src="html/images/toggle_off.png" />
        <File name="toggle_on" src="html/images/toggle_on.png" />
        <File name="toggle_right" src="html/images/toggle_right.png" />
        <File name="index" src="html/index.html" />
        <File name="jquery-3.2.1.min" src="html/js/jquery-3.2.1.min.js" />
        <File name="settings" src="html/js/settings.js" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
