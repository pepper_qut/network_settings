/* globals $, QiSession */

var behaviourManager = false
var connectionManager = false
var speechManager = false
var connecting = false

var clickEventType = ((document.ontouchstart !== null) ? 'click' : 'touchstart')
var currentVolume = 100

function getServicesJSON(services) {
  return services.map(function (service) {
    return array_reduce(service)
  })
}

function array_reduce(arr) {
  return arr.reduce(function (obj, field) {
    obj[field[0]] = Array.isArray(field[1]) ? array_reduce(field[1]) : field[1]
    return obj
  }, {})
}

$(document).ready(function () {
  new QiSession(function (session) {
    connected(session)
  }, disconnected)

  $('#exit').on(clickEventType, function () {
    if (!behaviourManager) return
    behaviourManager.stopBehavior('User/network/behavior_1')
  })
  $('#network-toggle').on(clickEventType, function () {
    if ($(this).attr('data-state') === 'wifi') {
      setTechnology('tethering')
    } else {
      setTechnology('wifi')
    }
  })
})

function connected (session) {
  console.log('Connected')

  session.service('ALBehaviorManager').then(function (service) {
    behaviourManager = service
  })
  session.service('ALTextToSpeech').then(function (service) {
    speechManager = service
  })

  session.service('ALMemory').then(function (service) {
    service.subscriber('NetworkStateChanged').then(function (subscriber) {
      subscriber.signal.connect(function () {
        update()
      })
    })
    service.subscriber('NetworkConnectStatus').then(function (subscriber) {
      subscriber.signal.connect(function (status) {
        if (!status[1]) {
          speechManager.say(status[2])
        }
        connecting = false
        update()
      })
    })
  })

  session.service('ALConnectionManager').then(function (connman) {
    connectionManager = connman
    update()
  })
}

function disconnected (session) {
  console.log('Disconnected')
}

function update () {
  if (!connectionManager) {
    return
  }
  
  connectionManager.getTetheringEnable('wifi').then(function (result) {
    if (result) {
      $('#network-toggle').attr('data-state', 'tethering')
      $('#network-toggle').attr('src', 'images/toggle_right.png')
    } else {
      $('#network-toggle').attr('data-state', 'wifi')
      $('#network-toggle').attr('src', 'images/toggle_left.png')
    }
  })

  connectionManager.services().then(function (services) {
    services = getServicesJSON(services)
    
    var current = undefined;
    for (i = 0; i < services.length; i++) {
      var service = services[i]
      
      if (service.Type === 'wifi' && (service.State === 'online' || service.State == 'ready')) {
        current = service
        break
      }
    }
    
    if (current != undefined) {
      $('#network-ip').html(current.IPv4.Address)
      $('.loading').hide()
      
    } else {
      connectionManager.getTetheringEnable('wifi').then(function (enabled) {
        if (enabled) {
          $('#network-ip').html('192.168.0.1')
        } else {
          if (connecting) {
            $('#network-ip').html('Connecting')
          } else {
            $('#network-ip').html('Disconnected')
          }
        }
        $('.loading').hide()
      })
    }
  }).catch(function (err) {
    console.log(err)
  })
}

function setTechnology(technology) {
  if (technology === 'tethering') {
    enableTethering()
  } else {
    enableWiFi()
  }
  $('.loading').show()
}

function enableTethering() {
  connectionManager.enableTethering('wifi', document.location.host, 'cyphy123').then(function () {
    connectionManager.getTetheringEnable('wifi').then(function (enabled) {
      if (enabled) {
        speechManager.say('Tethering is enabled')
        update()
      }
    })
  })
}

function disableTethering () {
  return new Promise(function (resolve, reject) {
    connectionManager.disableTethering('wifi').then(function () {
      connectionManager.getTetheringEnable('wifi').then(function (enabled) {
        if (!enabled) {
          resolve()
        } else {
          reject()
        }
      })
    })
  })
}

function enableWiFi() {
  disableTethering().then(function () {
    connectionManager.scan('wifi').then(function () {
      connectionManager.services().then(function (services) {
        services = getServicesJSON(services)

        for (i = 0; i < services.length; i++) {
          var service = services[i]

          if (service.Type != 'wifi') {
            continue
          }

          if (!service.Favorite) {
            continue
          }

          connectionManager.connect(service.ServiceId).then(function () {
            speechManager.say('WiFi is enabled')
            connecting = true
            update()
          })
          break
        }
      })
    }).catch(function (err) {
      console.log(err)
      enableWiFi()
    })
  }).catch(function (err) {
    enableWiFi()
  })
}